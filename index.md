---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: default
suppress_title: true # Triggers custom styling
---

<div class="post-list-content">

  <!-- FEATURED POST : begin -->
  <article class="featured-post m-has-thumb">

    <div class="post-image">
      <img src="files/img/klopp_lerro_train_sm.png" alt="Philip Klopp and Peter A. Lerro, Jr. Painting of a train in Richland, PA" style="margin: auto;display: block;">
    </div>
    <div class="post-core">
      <div class="post-date">
        Used with permission of Philip Klopp and Peter A. Lerro, Jr., Artist
      </div>
      <div class="post-excerpt">
        <p>
          Welcome to the online home of the borough of Richland, PA! Richland Borough, nestled in the Eastern part of Lebanon County, Pennsylvania, is a picturesque town named for its fertile soil. Town Sign Incorporated from Millcreek Township in 1906, the town is unique in that it has an active railroad crossing which intersects the town square. Main Street and Race Street, which also intersect at the square, link the northern and southern portions, as well as the eastern and western portions, of the borough. Thus, the crossing divides the entire town causing Richland to be mentioned in “Ripley’s Believe It or Not” books and on the televised game show “Jeopardy”.
        </p>
        <div class="post-date">
          Notice: Richland Borough does not have a Facebook page. Any Facebook page referencing Richland Borough is a page managed by private citizens, and does not reflect official information from the Richland Borough council, or its employees.
        </div>

        <ul id="lightSlider">
          {% for image in site.data.front_page_gallery %}
            <li data-thumb="./{{ image.THUMBNAIL_URL }}">
              <img src="./{{ image.PHOTO_URL }}" />
            </li>
          {% endfor %}
        </ul>

      </div>
    </div>
  </article>
  <!-- FEATURED POST : end -->

  <!--
  <article class="post">
    <h3 class="post-title">
      Learn More about Richland, PA
    </h3>
  </article>

  <article class="post">
    <h3 class="post-title">
      <a href="#">New Housing Complex in TownPress Nearly Complete</a>
    </h3>
    <div class="post-date">July 6, 2015</div>
  </article>
  -->

</div>
