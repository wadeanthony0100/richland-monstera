---
layout: default
suppress_title: false
title: Finances
---


<div class="card">
  <div class="card-header finance-header">
    Financial Records
  </div>
    <div id="collapse_{{ YEAR.YEAR_LABEL}}" class="collapse show">
      <div class="card-body">
  {% for RECORD in site.data.finances %}
          <div class="card-body">
            <div class="document-icon" title="PDF File"><i class="fas fa-file-download" aria-hidden="true"></i></div>
            <a class="finance-item" href="./{{ RECORD.LOCATION }}">{{ RECORD.LABEL }}</a>
          </div>
  {% endfor %}
      </div>
    </div>
</div>
